package org.kotlinlang.play
import java.util.*


object GameOX {
    var ox = arrayOf(
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " ")
    )

    var row = 0
    var column = 0
    var Round = 0
    var turn = "X"

    @JvmStatic
    fun main(args: Array<String>) {
        println("Game XO Start!!")
        while(true) {
            printTable()

            inputPosit()

            if (checkWin()) {
                break
            } else if (checkDraw()) {
                break
            }

            switchTurn()
        }

    }
    fun printTable() {
        println(" " + " 1 " + "2" + " 3 ")
        for (i in 0..2) {
            print(i + 1)
            for(j in 0..2) {
                print("|"+ ox[i][j])
            }
            print("|")
            println()
        }
    }
    fun inputPosit() {
        val input = Scanner(System.`in`)
        while(true) {
            print("Player put $turn at the position(row,column) :")
            try {
              val x = input.next()
              val y = input.next()
              row = x.toInt()
              column = y.toInt()

                if(row > 3 || row <= 0 || column > 3 || column <= 0) {
                    println("Row and Column must be number 1-3")
                    continue
                }
                row = row - 1
                column = column - 1
                if(ox[row][column] != " ") {
                    println("Row " + (row + 1) + " and Column " + (column + 1) + " can't choose again")
                    continue
                }
                Round++
                ox[row][column] = turn
                break
            } catch (x: Exception) {
                println("Row and Column must be number")
                continue
            }

        }
    }
    fun checkWin(): Boolean {
        var cheWin = false

        for (i in ox.indices) {
            if (ox[i][0] === turn && ox[i][1] === turn && ox[i][2] === turn
            ) {
                cheWin = true
            }
        }

        for (i in ox.indices) {
            if (ox[0][i] === turn && ox[1][i] === turn && ox[2][i] === turn
            ) {
                cheWin = true
            }
        }

        if (ox[0][0] === turn && ox[1][1] === turn && ox[2][2] === turn
        ) {
            cheWin = true
        }
        if (ox[0][2] === turn && ox[1][1] === turn && ox[2][0] === turn
        ) {
            cheWin = true
        }
        if (cheWin == true) {
            printTable()
            println("PLAYER $turn WIN!!")
            return true
        }
        return false
    }
    fun checkDraw(): Boolean {
        if (Round == 9) {
            printTable()
            println("DRAW~")
            return true
        }
        return false
    }
    fun switchTurn() {
        turn = if (turn === "X") {
            "O"
        } else {
            "X"
        }
    }

}
